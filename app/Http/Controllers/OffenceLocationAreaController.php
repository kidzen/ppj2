<?php

namespace App\Http\Controllers;

use App\OffenceLocationArea;
use Illuminate\Http\Request;

class OffenceLocationAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OffenceLocationArea  $offenceLocationArea
     * @return \Illuminate\Http\Response
     */
    public function show(OffenceLocationArea $offenceLocationArea)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OffenceLocationArea  $offenceLocationArea
     * @return \Illuminate\Http\Response
     */
    public function edit(OffenceLocationArea $offenceLocationArea)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OffenceLocationArea  $offenceLocationArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OffenceLocationArea $offenceLocationArea)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OffenceLocationArea  $offenceLocationArea
     * @return \Illuminate\Http\Response
     */
    public function destroy(OffenceLocationArea $offenceLocationArea)
    {
        //
    }
}
