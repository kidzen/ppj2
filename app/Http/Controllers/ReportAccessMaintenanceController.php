<?php

namespace App\Http\Controllers;

use App\ReportAccessMaintenance;
use Illuminate\Http\Request;

class ReportAccessMaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReportAccessMaintenance  $reportAccessMaintenance
     * @return \Illuminate\Http\Response
     */
    public function show(ReportAccessMaintenance $reportAccessMaintenance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReportAccessMaintenance  $reportAccessMaintenance
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportAccessMaintenance $reportAccessMaintenance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReportAccessMaintenance  $reportAccessMaintenance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportAccessMaintenance $reportAccessMaintenance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReportAccessMaintenance  $reportAccessMaintenance
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportAccessMaintenance $reportAccessMaintenance)
    {
        //
    }
}
