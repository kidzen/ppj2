<?php

namespace App\Http\Controllers;

use App\HandheldAssignment;
use Illuminate\Http\Request;

class HandheldAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HandheldAssignment  $handheldAssignment
     * @return \Illuminate\Http\Response
     */
    public function show(HandheldAssignment $handheldAssignment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HandheldAssignment  $handheldAssignment
     * @return \Illuminate\Http\Response
     */
    public function edit(HandheldAssignment $handheldAssignment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HandheldAssignment  $handheldAssignment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HandheldAssignment $handheldAssignment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HandheldAssignment  $handheldAssignment
     * @return \Illuminate\Http\Response
     */
    public function destroy(HandheldAssignment $handheldAssignment)
    {
        //
    }
}
