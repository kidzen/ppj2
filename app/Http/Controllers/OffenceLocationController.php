<?php

namespace App\Http\Controllers;

use App\OffenceLocation;
use Illuminate\Http\Request;

class OffenceLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OffenceLocation  $offenceLocation
     * @return \Illuminate\Http\Response
     */
    public function show(OffenceLocation $offenceLocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OffenceLocation  $offenceLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(OffenceLocation $offenceLocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OffenceLocation  $offenceLocation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OffenceLocation $offenceLocation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OffenceLocation  $offenceLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(OffenceLocation $offenceLocation)
    {
        //
    }
}
