<?php

namespace App\Http\Controllers;

use App\OffenceRateMaster;
use Illuminate\Http\Request;

class OffenceRateMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OffenceRateMaster  $offenceRateMaster
     * @return \Illuminate\Http\Response
     */
    public function show(OffenceRateMaster $offenceRateMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OffenceRateMaster  $offenceRateMaster
     * @return \Illuminate\Http\Response
     */
    public function edit(OffenceRateMaster $offenceRateMaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OffenceRateMaster  $offenceRateMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OffenceRateMaster $offenceRateMaster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OffenceRateMaster  $offenceRateMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(OffenceRateMaster $offenceRateMaster)
    {
        //
    }
}
