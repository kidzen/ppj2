<?php

namespace App\Http\Controllers;

use App\ReportMaintenance;
use Illuminate\Http\Request;

class ReportMaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReportMaintenance  $reportMaintenance
     * @return \Illuminate\Http\Response
     */
    public function show(ReportMaintenance $reportMaintenance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReportMaintenance  $reportMaintenance
     * @return \Illuminate\Http\Response
     */
    public function edit(ReportMaintenance $reportMaintenance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReportMaintenance  $reportMaintenance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportMaintenance $reportMaintenance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReportMaintenance  $reportMaintenance
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportMaintenance $reportMaintenance)
    {
        //
    }
}
