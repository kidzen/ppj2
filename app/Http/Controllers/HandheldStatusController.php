<?php

namespace App\Http\Controllers;

use App\HandheldStatus;
use Illuminate\Http\Request;

class HandheldStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HandheldStatus  $handheldStatus
     * @return \Illuminate\Http\Response
     */
    public function show(HandheldStatus $handheldStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HandheldStatus  $handheldStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(HandheldStatus $handheldStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HandheldStatus  $handheldStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HandheldStatus $handheldStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HandheldStatus  $handheldStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(HandheldStatus $handheldStatus)
    {
        //
    }
}
