<?php

namespace App\Http\Controllers;

use App\OffenceAct;
use Illuminate\Http\Request;

class OffenceActController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OffenceAct  $offenceAct
     * @return \Illuminate\Http\Response
     */
    public function show(OffenceAct $offenceAct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OffenceAct  $offenceAct
     * @return \Illuminate\Http\Response
     */
    public function edit(OffenceAct $offenceAct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OffenceAct  $offenceAct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OffenceAct $offenceAct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OffenceAct  $offenceAct
     * @return \Illuminate\Http\Response
     */
    public function destroy(OffenceAct $offenceAct)
    {
        //
    }
}
