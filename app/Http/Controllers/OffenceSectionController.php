<?php

namespace App\Http\Controllers;

use App\OffenceSection;
use Illuminate\Http\Request;

class OffenceSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OffenceSection  $offenceSection
     * @return \Illuminate\Http\Response
     */
    public function show(OffenceSection $offenceSection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OffenceSection  $offenceSection
     * @return \Illuminate\Http\Response
     */
    public function edit(OffenceSection $offenceSection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OffenceSection  $offenceSection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OffenceSection $offenceSection)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OffenceSection  $offenceSection
     * @return \Illuminate\Http\Response
     */
    public function destroy(OffenceSection $offenceSection)
    {
        //
    }
}
