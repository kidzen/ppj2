<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function rename()
    {
        $directories = array_slice(scandir(resource_path('themes/metronic-v52/demo')),2);
        foreach ($directories as $i => $value) {
            $fileList = scandir(resource_path('themes/metronic-v52/demo/'.$value));
            foreach ($fileList as $j => $file) {
                $ex = '.html';
                if(strpos($file, $ex)){
                    $fileName = resource_path('themes/metronic-v52/demo/'.$value.'/'.$file);
                    $newName = basename($fileName,$ex).'.blade.php';
                    $newFile = resource_path('themes/metronic-v52/demo/'.$value.'/'.$newName);
                    // var_dump($fileName);
                    var_dump($newFile);
                    var_dump($fileName);
                    rename($fileName,$newFile);
                    // die;
                }
            }
        }
        die;

        return view('home');
    }
}
