<?php

namespace App\Http\Controllers;

use App\NoticeMaintenanceReporting;
use Illuminate\Http\Request;

class NoticeMaintenanceReportingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NoticeMaintenanceReporting  $noticeMaintenanceReporting
     * @return \Illuminate\Http\Response
     */
    public function show(NoticeMaintenanceReporting $noticeMaintenanceReporting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NoticeMaintenanceReporting  $noticeMaintenanceReporting
     * @return \Illuminate\Http\Response
     */
    public function edit(NoticeMaintenanceReporting $noticeMaintenanceReporting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NoticeMaintenanceReporting  $noticeMaintenanceReporting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NoticeMaintenanceReporting $noticeMaintenanceReporting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NoticeMaintenanceReporting  $noticeMaintenanceReporting
     * @return \Illuminate\Http\Response
     */
    public function destroy(NoticeMaintenanceReporting $noticeMaintenanceReporting)
    {
        //
    }
}
