<?php

namespace App\Http\Controllers;

use App\OffenceNoticeMaintenance;
use Illuminate\Http\Request;

class OffenceNoticeMaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OffenceNoticeMaintenance  $offenceNoticeMaintenance
     * @return \Illuminate\Http\Response
     */
    public function show(OffenceNoticeMaintenance $offenceNoticeMaintenance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OffenceNoticeMaintenance  $offenceNoticeMaintenance
     * @return \Illuminate\Http\Response
     */
    public function edit(OffenceNoticeMaintenance $offenceNoticeMaintenance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OffenceNoticeMaintenance  $offenceNoticeMaintenance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OffenceNoticeMaintenance $offenceNoticeMaintenance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OffenceNoticeMaintenance  $offenceNoticeMaintenance
     * @return \Illuminate\Http\Response
     */
    public function destroy(OffenceNoticeMaintenance $offenceNoticeMaintenance)
    {
        //
    }
}
