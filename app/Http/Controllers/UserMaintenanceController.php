<?php

namespace App\Http\Controllers;

use App\UserMaintenance;
use Illuminate\Http\Request;

class UserMaintenanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserMaintenance  $userMaintenance
     * @return \Illuminate\Http\Response
     */
    public function show(UserMaintenance $userMaintenance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserMaintenance  $userMaintenance
     * @return \Illuminate\Http\Response
     */
    public function edit(UserMaintenance $userMaintenance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserMaintenance  $userMaintenance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserMaintenance $userMaintenance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserMaintenance  $userMaintenance
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserMaintenance $userMaintenance)
    {
        //
    }
}
