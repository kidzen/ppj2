create table [dbo].[access_level](
	[code] [smallint] identity(1,1) not null,
	[description] [varchar](32) not null,
	[created_by] [varchar](32) not null constraint [df__access_le__creat__4589517f]  default (user_name()),
	[created_date] [datetime] not null constraint [df__access_le__creat__467d75b8]  default (getdate()),
 constraint [pk_access_level] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[handheld_assignment](
	[code] [int] identity(1,1) not null,
	[handheld_code] [int] null,
	[officer_code] [int] null,
	[start_assignment_date] [datetime] not null,
	[end_assignment_date] [datetime] not null,
	[handheld_status_code] [smallint] null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
 constraint [pk_handheld_assignment] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary],
 constraint [un_handheld_code_assignment] unique nonclustered 
(
	[handheld_code] asc,
	[start_assignment_date] asc,
	[end_assignment_date] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary],
 constraint [un_officer_code_assignment] unique nonclustered 
(
	[officer_code] asc,
	[start_assignment_date] asc,
	[end_assignment_date] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[handheld_maintenance](
	[code] [int] identity(1,1) not null,
	[id] [varchar](3) not null,
	[serial_no] [varchar](19) not null,
	[status_code] [smallint] not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_handheld_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[handheld_status](
	[code] [smallint] identity(1,1) not null,
	[description] [varchar](32) not null,
	[created_by] [varchar](32) not null constraint [df__handheld___creat__4e1e9780]  default (user_name()),
	[created_date] [datetime] not null constraint [df__handheld___creat__4f12bbb9]  default (getdate()),
 constraint [pk_handheld_status] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[notice_maintenance_reporting](
	[id] [int] identity(1,1) not null,
	[notices_issued] [int] not null,
	[notice_offence_date] [datetime] not null,
	[description] [varchar](200) null,
	[offence_location_area] [varchar](200) null,
	[offence_location] [varchar](200) null,
	[offence_act_code] [tinyint] null,
	[offence_section_code] [tinyint] null,
 constraint [pk_notice_maintenance_reporting] primary key clustered 
(
	[id] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_act](
	[code] [int] identity(1,1) not null,
	[description] [varchar](512) not null,
	[short_description] [varchar](128) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_offence_act] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_location](
	[offence_location_area_code] [int] not null,
	[code] [int] not null,
	[description] [varchar](100) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_offence_location] primary key clustered 
(
	[offence_location_area_code] asc,
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_location_area](
	[code] [int] identity(1,1) not null,
	[description] [varchar](50) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_offence_location_area] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_notice_maintenance](
	[id] [uniqueidentifier] not null,
	[notice_no] [varchar](20) null,
	[vehicle_no] [varchar](20) null,
	[offence_date] [datetime] null,
	[officer_id] [varchar](10) null,
	[handheld_code] [int] null,
	[vehicle_type] [varchar](20) null,
	[vehicle_color] [varchar](20) null,
	[vehicle_make_model] [varchar](100) null,
	[road_tax_no] [varchar](20) null,
	[offence_code] [varchar](20) null,
	[offence_location_area] [varchar](50) null,
	[offence_location] [varchar](100) null,
	[offence_location_details] [varchar](100) null,
	[compound_amount] [float] null,
	[compound_expiry_date] [date] null,
	[witness] [varchar](100) null,
	[image_name1] [varchar](30) null,
	[image_name2] [varchar](30) null,
	[image_name3] [varchar](30) null,
	[created_date] [date] null,
	[offence_notice_status] [char](1) null,
	[is_sent] [bit] null,
	[sent_date] [datetime] null,
 constraint [pk_offence_notice_maintenance] primary key clustered 
(
	[id] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_rate_master](
	[sno] [int] identity(1,1) not null,
	[act_code] [int] not null,
	[section_code] [int] not null,
	[zone1] [int] null,
	[amount1] [decimal](18, 2) null,
	[amount11] [decimal](18, 2) null,
	[amount_desc1] [varchar](100) null,
	[zone2] [int] null,
	[amount2] [decimal](18, 2) null,
	[amount21] [decimal](18, 2) null,
	[amount_desc2] [varchar](100) null,
	[zone3] [int] null,
	[amount3] [decimal](18, 2) null,
	[amount31] [decimal](18, 2) null,
	[amount_desc3] [varchar](100) null,
	[zone4] [int] null,
	[amount4] [decimal](18, 2) null,
	[amount41] [decimal](18, 2) null,
	[amount_desc4] [varchar](100) null,
	[max_compound_amount] [decimal](18, 2) null,
 constraint [pk_offence_rate_master] primary key clustered 
(
	[sno] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_section](
	[offence_act_code] [int] not null,
	[code] [int] not null,
	[id] [varchar](20) not null,
	[no] [varchar](24) not null,
	[subsection_no] [varchar](24) null,
	[description] [varchar](1024) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_offence_section] primary key clustered 
(
	[offence_act_code] asc,
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[officer_maintenance](
	[code] [int] identity(1,1) not null,
	[id] [varchar](10) not null,
	[rank] [smallint] not null,
	[rank_no] [varchar](6) null,
	[name] [varchar](80) not null,
	[password] [varchar](64) not null,
	[password_changed_by] [int] not null,
	[last_password_change_date] [datetime] not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_officer_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[officer_rank](
	[code] [smallint] identity(1,1) not null,
	[rank] [varchar](20) not null,
	[has_rank_no] [bit] not null,
	[created_by] [varchar](32) not null constraint [df__officer_r__creat__3429bb53]  default (user_name()),
	[created_date] [datetime] not null constraint [df__officer_r__creat__351ddf8c]  default (getdate()),
 constraint [pk_officer_rank] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[report_access_maintenance](
	[code] [int] identity(1,1) not null,
	[report_id] [varchar](12) not null,
	[access_code] [smallint] not null,
	[created_by] [int] null,
	[created_date] [datetime] not null,
 constraint [pk_report_access_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[report_maintenance](
	[code] [varchar](12) not null,
	[name] [varchar](80) null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[is_deleted] [bit] null constraint [df_report_maintenance_is_deleted]  default ((0)),
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[filename] [varchar](80) null,
	[desc] [int] null,
 constraint [pk_report_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[system_settings](
	[report_date] [date] null
) on [primary]


create table [dbo].[user_maintenance](
	[code] [int] identity(1,1) not null,
	[id] [varchar](6) not null,
	[name] [varchar](80) not null,
	[password] [varchar](64) not null,
	[access_level] [smallint] not null,
	[last_login_date] [datetime] not null,
	[last_password_change_date] [datetime] not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_user_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[vehicle_make](
	[code] [int] identity(1,1) not null,
	[description] [varchar](128) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_vehicle_make] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[vehicle_model](
	[vehicle_make_code] [int] not null,
	[code] [int] not null,
	[description] [varchar](64) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_vehicle_model] primary key clustered 
(
	[vehicle_make_code] asc,
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[vehicle_type](
	[code] [int] identity(1,1) not null,
	[description] [varchar](64) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_vehicle_type] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___creat__7073af84] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[handheld_assignment] check constraint [fk__handheld___creat__7073af84]

alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___handh__22401542] foreign key([handheld_code])
references [dbo].[handheld_maintenance] ([code])

-- alter table [dbo].[handheld_assignment] check constraint [fk__handheld___handh__22401542]

alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___handh__725bf7f6] foreign key([handheld_status_code])
references [dbo].[handheld_status] ([code])

-- alter table [dbo].[handheld_assignment] check constraint [fk__handheld___handh__725bf7f6]

alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___offic__73501c2f] foreign key([officer_code])
references [dbo].[officer_maintenance] ([code])

-- alter table [dbo].[handheld_assignment] check constraint [fk__handheld___offic__73501c2f]

alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___updat__74444068] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[handheld_assignment] check constraint [fk__handheld___updat__74444068]

alter table [dbo].[handheld_maintenance]  with check add  constraint [fk__handheld___creat__1b9317b3] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[handheld_maintenance] check constraint [fk__handheld___creat__1b9317b3]

alter table [dbo].[handheld_maintenance]  with check add  constraint [fk__handheld___delet__1d7b6025] foreign key([deleted_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[handheld_maintenance] check constraint [fk__handheld___delet__1d7b6025]

alter table [dbo].[handheld_maintenance]  with check add  constraint [fk__handheld___statu__1a9ef37a] foreign key([status_code])
references [dbo].[handheld_status] ([code])

-- alter table [dbo].[handheld_maintenance] check constraint [fk__handheld___statu__1a9ef37a]

alter table [dbo].[handheld_maintenance]  with check add  constraint [fk__handheld___updat__1c873bec] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[handheld_maintenance] check constraint [fk__handheld___updat__1c873bec]

alter table [dbo].[offence_act]  with check add  constraint [fk__offence_a__creat__7dcdaaa2] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_act] check constraint [fk__offence_a__creat__7dcdaaa2]

alter table [dbo].[offence_act]  with check add  constraint [fk__offence_a__delet__7ec1cedb] foreign key([deleted_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_act] check constraint [fk__offence_a__delet__7ec1cedb]

alter table [dbo].[offence_act]  with check add  constraint [fk__offence_a__updat__7fb5f314] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_act] check constraint [fk__offence_a__updat__7fb5f314]

alter table [dbo].[offence_location]  with check add  constraint [fk__offence_l__creat__056ecc6a] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_location] check constraint [fk__offence_l__creat__056ecc6a]

alter table [dbo].[offence_location]  with check add  constraint [fk__offence_l__delet__0662f0a3] foreign key([deleted_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_location] check constraint [fk__offence_l__delet__0662f0a3]

alter table [dbo].[offence_location]  with check add  constraint [fk__offence_l__offen__075714dc] foreign key([offence_location_area_code])
references [dbo].[offence_location_area] ([code])

-- alter table [dbo].[offence_location] check constraint [fk__offence_l__offen__075714dc]

alter table [dbo].[offence_location]  with check add  constraint [fk__offence_l__updat__084b3915] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_location] check constraint [fk__offence_l__updat__084b3915]

alter table [dbo].[offence_location_area]  with check add  constraint [fk__offence_l__creat__093f5d4e] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_location_area] check constraint [fk__offence_l__creat__093f5d4e]

alter table [dbo].[offence_location_area]  with check add  constraint [fk__offence_l__delet__0a338187] foreign key([deleted_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_location_area] check constraint [fk__offence_l__delet__0a338187]

alter table [dbo].[offence_location_area]  with check add  constraint [fk__offence_l__updat__0b27a5c0] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_location_area] check constraint [fk__offence_l__updat__0b27a5c0]

alter table [dbo].[offence_rate_master]  with check add  constraint [fk_offence_rate_master_act_code] foreign key([act_code])
references [dbo].[offence_act] ([code])

-- alter table [dbo].[offence_rate_master] check constraint [fk_offence_rate_master_act_code]

alter table [dbo].[offence_rate_master]  with check add  constraint [fk_offence_rate_master_offence_section] foreign key([act_code], [section_code])
references [dbo].[offence_section] ([offence_act_code], [code])

-- alter table [dbo].[offence_rate_master] check constraint [fk_offence_rate_master_offence_section]

alter table [dbo].[offence_section]  with check add  constraint [fk__offence_s__creat__178d7ca5] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_section] check constraint [fk__offence_s__creat__178d7ca5]

alter table [dbo].[offence_section]  with check add  constraint [fk__offence_s__delet__1881a0de] foreign key([deleted_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_section] check constraint [fk__offence_s__delet__1881a0de]

alter table [dbo].[offence_section]  with check add  constraint [fk__offence_s__offen__1975c517] foreign key([offence_act_code])
references [dbo].[offence_act] ([code])

-- alter table [dbo].[offence_section] check constraint [fk__offence_s__offen__1975c517]

alter table [dbo].[offence_section]  with check add  constraint [fk__offence_s__updat__1a69e950] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[offence_section] check constraint [fk__offence_s__updat__1a69e950]

alter table [dbo].[report_access_maintenance]  with check add  constraint [fk_ram_access_code__al_code] foreign key([access_code])
references [dbo].[access_level] ([code])

-- alter table [dbo].[report_access_maintenance] check constraint [fk_ram_access_code__al_code]

alter table [dbo].[report_access_maintenance]  with check add  constraint [fk_ram_created_by__um_code] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[report_access_maintenance] check constraint [fk_ram_created_by__um_code]

alter table [dbo].[report_access_maintenance]  with check add  constraint [fk_ram_report_id__rm_code] foreign key([report_id])
references [dbo].[report_maintenance] ([code])

-- alter table [dbo].[report_access_maintenance] check constraint [fk_ram_report_id__rm_code]

alter table [dbo].[report_maintenance]  with check add  constraint [fk_rm_created_by__um_code] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[report_maintenance] check constraint [fk_rm_created_by__um_code]

alter table [dbo].[report_maintenance]  with check add  constraint [fk_rm_updated_by__um_code] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[report_maintenance] check constraint [fk_rm_updated_by__um_code]

alter table [dbo].[user_maintenance]  with check add  constraint [fk__user_main__acces__23f3538a] foreign key([access_level])
references [dbo].[access_level] ([code])

-- alter table [dbo].[user_maintenance] check constraint [fk__user_main__acces__23f3538a]

alter table [dbo].[vehicle_make]  with check add  constraint [fk__vehicle_m__creat__24e777c3] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[vehicle_make] check constraint [fk__vehicle_m__creat__24e777c3]

alter table [dbo].[vehicle_make]  with check add  constraint [fk__vehicle_m__delet__25db9bfc] foreign key([deleted_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[vehicle_make] check constraint [fk__vehicle_m__delet__25db9bfc]

alter table [dbo].[vehicle_make]  with check add  constraint [fk__vehicle_m__updat__26cfc035] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[vehicle_make] check constraint [fk__vehicle_m__updat__26cfc035]

alter table [dbo].[vehicle_model]  with check add  constraint [fk__vehicle_m__creat__27c3e46e] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[vehicle_model] check constraint [fk__vehicle_m__creat__27c3e46e]

alter table [dbo].[vehicle_model]  with check add  constraint [fk__vehicle_m__delet__28b808a7] foreign key([deleted_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[vehicle_model] check constraint [fk__vehicle_m__delet__28b808a7]

alter table [dbo].[vehicle_model]  with check add  constraint [fk__vehicle_m__updat__29ac2ce0] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[vehicle_model] check constraint [fk__vehicle_m__updat__29ac2ce0]

alter table [dbo].[vehicle_model]  with check add  constraint [fk__vehicle_m__vehic__2aa05119] foreign key([vehicle_make_code])
references [dbo].[vehicle_make] ([code])

-- alter table [dbo].[vehicle_model] check constraint [fk__vehicle_m__vehic__2aa05119]

alter table [dbo].[vehicle_type]  with check add  constraint [fk__vehicle_t__creat__2b947552] foreign key([created_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[vehicle_type] check constraint [fk__vehicle_t__creat__2b947552]

alter table [dbo].[vehicle_type]  with check add  constraint [fk__vehicle_t__delet__2c88998b] foreign key([deleted_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[vehicle_type] check constraint [fk__vehicle_t__delet__2c88998b]

alter table [dbo].[vehicle_type]  with check add  constraint [fk__vehicle_t__updat__2d7cbdc4] foreign key([updated_by])
references [dbo].[user_maintenance] ([code])

-- alter table [dbo].[vehicle_type] check constraint [fk__vehicle_t__updat__2d7cbdc4]



-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[addreportsforaccesslevel] 
	@accesslevel int,
	@reportid varchar(12)
	as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    insert into dbo.report_access_maintenance (report_id,access_code,created_date) values (@reportid,@accesslevel,getdate())

end






/*
-- =============================================
-- author:		naveen
-- create date: 18/09/2008
-- description:	checks for existing user.
-- =============================================

[checkforuser] '545'
*/
create procedure [dbo].[checkforuser]
	@userid nvarchar(6)
as
begin
-- set nocount on added to prevent extra result sets from
-- interfering with select statements.
set nocount on;
declare	@returnvalue nvarchar(6)


set @returnvalue =0

	--user exists and cannot be created
	set @returnvalue = (select 1 
	from dbo.user_maintenance
	where id = @userid
	and created_date = deleted_date)
	
--techm changed 17/06/09
--	if (isnull(@returnvalue,0) !=1)
-- 
--	begin
--	--user has been deleted and can be recreated
--	set @returnvalue = (select 0 
--	from dbo.user_maintenance
--	where id = @userid
--	and created_date != deleted_date
--	and exists (select 1 from dbo.officer_maintenance
--	where id = @userid)
--	union
--	select 0 from dbo.officer_maintenance
--	where id = @userid and not exists
--	(select 1 from dbo.user_maintenance
--	where id = @userid))
--	end
   
select isnull(@returnvalue,0)

end


set quoted_identifier on




/*
-- =============================================
-- author:		naveen
-- create date: 18/09/2008
-- description:	creates a new user.
-- =============================================

[createnewuser] '00000000000', 'somen1', 'asa07wqwas:', 1, 1
*/

create procedure [dbo].[createnewuser]
	@userid nvarchar(6), 
	@name nvarchar(80),
	@password nvarchar(64),
	@accesslevel int, 
	@createdby int
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

	insert into dbo.user_maintenance
				(id, 
				 [name],
				 password,
				 access_level,
				 last_login_date,
				 last_password_change_date,
				 created_by,
				 created_date,
				 updated_by,
				 updated_date,
				 deleted_by,
				 deleted_date)

	values
			(lower(@userid), 
			 @name, 
			 @password,
			 @accesslevel, 
			 getdate(),
			 getdate(),
			 @createdby, 
			 getdate(),
			 1,
			 getdate(),
			 1, 
			 getdate())
end





set quoted_identifier on




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[deletenoticemaintenacereportexisted]
	-- add the parameters for the stored procedure here
	@noticeoffencedate date
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	delete from dbo.notice_maintenance_reporting 
	where convert(date, notice_offence_date) = @noticeoffencedate
end





-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[deletereportsforaccesslevel] 
	@accesslevel int
	as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    delete from dbo.report_access_maintenance where access_code = @accesslevel

end






-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[deleteuser]
	-- add the parameters for the stored procedure he
	@code int,
	@loggedinuser int
as
begin
	update dbo.user_maintenance set deleted_by=@loggedinuser, deleted_date = getdate() where code=@code
end



set quoted_identifier on





-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getallaccesslevels] 
	as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    select code,description from dbo.access_level

end







-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getallacts] 
	
	as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    select code, description, short_description from dbo.offence_act
end









-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getallreports] 
	as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    select code,name,filename from dbo.report_maintenance where is_deleted =  0

end









-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getallusers] 
	as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    select code,id,name,access_level from dbo.user_maintenance

end








-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getautocompletenoticenos]
	-- add the parameters for the stored procedure here
	@noticeno varchar(50)
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select top 20 notice_no as [no] from offence_notice_maintenance where notice_no like '%' + @noticeno + '%'
end



-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getdataforintegrationprocess]
	-- add the parameters for the stored procedure here
	
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select onm.id, onm.notice_no, onm.vehicle_no, onm.vehicle_type, onm.vehicle_color,
	orm.max_compound_amount as compound_amount, os.[description], onm.offence_code,
	onm.offence_location + ', ' + onm.offence_location_area + ', w.p. putrajaya' as offence_location,
	onm.officer_id, onm.road_tax_no, onm.offence_date, onm.created_date
	from offence_notice_maintenance onm
	left join offence_section os on onm.offence_code = os.id
	left join offence_rate_master orm on os.code = orm.section_code and os.offence_act_code = orm.act_code
	where onm.is_sent is null or onm.is_sent = 0

end



-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getdatanoticereportingupdatedtoday]
	-- add the parameters for the stored procedure here
	@updateddate datetime
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select * from dbo.offence_notice_maintenance 
	where 
		cast(convert(varchar(10), created_date, 110) as date) = @updateddate
end




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getenforcementid]
	-- add the parameters for the stored procedure here
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select (rtrim(om.id) + ' - ' + rtrim(or2.rank) + ' ' + rtrim(om.rank_no) + ' ' + rtrim(om.name)) as value, om.code, om.id, om.name
	from officer_maintenance as om,officer_rank as or2  
	where om.rank = or2.code and (om.created_date = om.deleted_date
	or om.id in

	(select distinct officer_id from offence_notice_maintenance))
	order by om.id
end

-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getidhandheld]
	-- add the parameters for the stored procedure here
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select (id + ' - ' + serial_no) as [description], code 
	from handheld_maintenance 
	where created_date = deleted_date
	order by id
end




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getlogindetails] 
	@login varchar(20)
	as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;
	
 
  select um.name, um.password, um.access_level,um.code, al.description 
  from dbo.user_maintenance um inner join dbo.access_level al
  on um.access_level= al.code
	and um.id = rtrim(ltrim(@login))
	and um.deleted_date = um.created_date


end

set quoted_identifier on




create procedure [dbo].[getnoticereporting]    
      @offencedate datetime
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select 
		oa.description,
		onm.offence_location_area,
		case when offence_location is null or offence_location = '' then 'dan lain-lain' else offence_location end as offence_location, 
		os.code as offence_section_code,
		os.offence_act_code,
		cast(convert(varchar(10), offence_date, 110) as date) as offence_date,
		count(*) as notices_issued
	from 
		offence_notice_maintenance onm 
		inner join offence_section os on onm.offence_code = os.id and os.created_date = os.deleted_date
		inner join offence_act oa on os.offence_act_code = oa.code
	where 
		cast(convert(varchar(10), offence_date, 110) as date) = @offencedate
	group by 
		os.offence_act_code, os.code,
		oa.description,
		onm.offence_location_area,
		case when offence_location is null or offence_location = '' then 'dan lain-lain' else offence_location end,
		cast(convert(varchar(10), offence_date, 110) as date)
end



-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getoffenceact]
	-- add the parameters for the stored procedure here
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select rtrim(short_description) as [description], code, [description] as fulldescription from offence_act 
	where (convert(varchar(11), created_date) = convert(varchar(11), deleted_date))
	order by code
end




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getoffencelocation]
	-- add the parameters for the stored procedure here
	@offencelocationareacode int
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select (rtrim(description)) as value, code, offence_location_area_code 
	from offence_location 
	where offence_location_area_code = @offencelocationareacode
	order by [description]
end




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getoffencelocationarea]
	-- add the parameters for the stored procedure here
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select code, [description] 
	from offence_location_area 
	where created_date = deleted_date
	order by description
end




create procedure [dbo].[getoffencerate]
    @offencecode varchar(100)
as
begin
select * from  offence_rate_master
inner join offence_section on offence_rate_master.act_code = offence_section.offence_act_code and
offence_rate_master.section_code = offence_section.code
where offence_section.id = @offencecode
end




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getoffencesection]
	-- add the parameters for the stored procedure here
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select no, subsection_no, code, offence_act_code, description 
	from offence_section 
	where created_date = deleted_date
	order by offence_act_code, code asc
end





create procedure [dbo].[getrep0001data]
	@startdate datetime,
	@enddate datetime,
	@offenceact int
as
begin

create table #temp (offence_section_order int,
					[notis kompaun dikeluarkan] int,
					section_description varchar(1050),
					section_code int)
insert into #temp   
	select 
		b.offence_section_order,    
        count(a.notice_no),
		b.no + ' ' + b.subsection_no + ' - ' + b.[description],
		b.code
	from  
		offence_notice_maintenance a
		left join (
			select top 100 percent *,
				case when isnumeric(substring(no, charindex(' ', no) + 1, len(no)-charindex(' ', no))) = 1 
					then convert(int, substring(no, charindex(' ', no) + 1, len(no)-charindex(' ', no)))
					when isnumeric(substring(no, charindex(' ', no) + 1, len(no)-charindex(' ', no) - 1)) = 1 
					then convert(int, substring(no, charindex(' ', no) + 1, len(no)-charindex(' ', no) - 1))
					when isnumeric(substring(no, charindex(' ', no) + 1, len(no)-charindex(' ', no))) <> 1 
					then 0
				end as offence_section_order
			from dbo.offence_section
			where  offence_section.created_date = offence_section.deleted_date
			order by offence_section_order
		) b on a.offence_code = b.id
	where 
		a.offence_date >= @startdate and a.offence_date < dateadd(day, 1, convert(varchar(12), @enddate, 101))
		and b.offence_act_code = @offenceact
	group by 
		isnull(b.no, ' ') + ' ' + isnull(b.subsection_no, ' ') + isnull(b.description, ' '), offence_section_order, b.code, b.no + ' ' + b.subsection_no + ' - ' + b.[description]
	order by b.code
	
select 
	section_description as [seksyen kesalahan],
	[notis kompaun dikeluarkan] as [notis kompaun]
	 from #temp 
union all
select 'jumlah',sum("notis kompaun dikeluarkan")
from #temp 
drop table #temp

end



create procedure [dbo].[getrep0002data]
	@startdate datetime,
	@enddate datetime,
	@offencelocation varchar(100),
	@offencearea varchar(100)
as
begin
	declare @ssql nvarchar(max)
	declare @description varchar(110)
	declare @final varchar(max)
	declare @finalt varchar(max)
	declare @finalr varchar(max)
	declare @finala varchar(max)
	declare @finalsum int
	declare @lengthofstring int
	declare act_cursor cursor for 

	select a.description
	from dbo.offence_act a

	set @final = ' '
	set @finalt = ' '
	set @finalr = ' '
	set @finala = ' '
	set @finalsum = 0

	open act_cursor

		fetch next from act_cursor
		into @description 
		while @@fetch_status = 0
		begin
			set @final = @final + ', [' +  @description +']'
			set @finalt = @finalt + ', sum(isnull([' +  @description +'], 0))'
			set @finalr = @finalr + ' isnull([' +  @description +'], 0) +'
			set @finala = @finala + ', isnull([' +  @description +'], 0) as [' +  @description +']'
			fetch next from act_cursor 
			into @description
		end 
	close act_cursor

	deallocate act_cursor

	set @finalr = @finalr + ' 0 '

	if(@final is null or @final = '')
	begin
	 return 0	
	end

	select @ssql = ' select area, location' + @finala +','	+@finalr+ 'as jumlah from 
	(select nmr.offence_location_area as area, nmr.offence_location as location, oa.description as act, sum(nmr.notices_issued) as notice_count from 
	notice_maintenance_reporting nmr
	inner join offence_act oa on oa.code = nmr.offence_act_code 
	where nmr.notice_offence_date >= ''' + convert(varchar(12), @startdate, 101) +
	''' and nmr.notice_offence_date < dateadd(day, 1, ''' + convert(varchar(12), @enddate, 101) + ''')
	and (
		(''' + @offencearea + ''' <> '''' and ''' + @offencelocation + ''' = ''''
			and offence_location in (
				select [description]      
				from offence_location
				where offence_location_area_code in (select code from offence_location_area where [description] = ''' + @offencearea + ''')))
		or (''' + @offencelocation + ''' <> '''' and offence_location = ''' + @offencelocation + ''')
		or (''' + @offencelocation + ''' = '''' and ''' + @offencearea + ''' = ''''))
	and (''' + @offencearea + ''' <> '''' and offence_location_area = ''' + @offencearea + '''
		or (''' + @offencearea + ''' = ''''))
	group by nmr.offence_location_area, nmr.offence_location, oa.description ) p
	pivot
	(sum (notice_count)
	for act in
	( ' + substring(@final,3,len(@final)) + ')) as pvt

	union all
			 
	select ''jumlah'', '''''+ @finalt +','+replace(@finalt,', sum','+ sum')+ ' from 
	(select nmr.offence_location_area as area, nmr.offence_location as location, oa.description as act, sum(nmr.notices_issued) as notice_count from 
	notice_maintenance_reporting nmr
	inner join offence_act oa on oa.code = nmr.offence_act_code 
	where nmr.notice_offence_date >= ''' + convert(varchar(12), @startdate, 101) +
	''' and nmr.notice_offence_date < dateadd(day, 1, ''' + convert(varchar(12), @enddate, 101) + ''')
	and (
		(''' + @offencearea + ''' <> '''' and ''' + @offencelocation + ''' = ''''
			and offence_location in (
				select [description]      
				from offence_location
				where offence_location_area_code in (select code from offence_location_area where [description] = ''' + @offencearea + ''')))
		or (''' + @offencelocation + ''' <> '''' and offence_location = ''' + @offencelocation + ''')
		or (''' + @offencelocation + ''' = '''' and ''' + @offencearea + ''' = ''''))
	and (''' + @offencearea + ''' <> '''' and offence_location_area = ''' + @offencearea + '''
		or (''' + @offencearea + ''' = ''''))
	group by nmr.offence_location_area, nmr.offence_location, oa.description ) p
	pivot
	(sum (notice_count)
	for act in
	( ' + substring(@final, 3, len(@final)) + ')) as pvt'

	print @ssql
	exec sp_executesql @ssql

end





create procedure [dbo].[getrep0003data]
-- add the parameters for the stored procedure here
	@startrowindex	int,
	@maxrows		int,
	@wherequery		nvarchar(max)
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;
	declare @firstrec int, @lastrec int
	declare @fromto nvarchar(max)
	
	if @maxrows <> 0
	begin
		select @firstrec = ((@startrowindex - 1) * @maxrows) + 1
		select @lastrec = @startrowindex * @maxrows
		
		if (@startrowindex = 1)
		begin
			set @firstrec = 1
			set @lastrec = @maxrows
		end
		
		set @fromto = ' where rownumber between @startrowindex and @maxrows'
	end
	else
		set @fromto = ''
	
	declare @temptable table([notis] varchar(100),
				    [trkh k'slhan] varchar(150),
				    [jenis kend] varchar(150),
				    [no kend] varchar(150),
				    [no cukai jln] varchar(150),
					[peruntukan undang2] varchar(512),
					[seksyen k'slhan] varchar(50),
					[kawasan] varchar(50),
					[tpt/jln k'slhan] varchar(150))
					
				    
	declare @query nvarchar(max)
	set @query = 'select [notis],
		[trkh k''slhan],
		[jenis kend],
		[no kend],
		[no cukai jln],
		[peruntukan undang2],
		[seksyen k''slhan],
		[kawasan],
		[tpt/jln k''slhan]
	from (
		select row_number() over (order by a.[notice_no]) as rownumber,
		a.[notice_no] as [notis],
		substring(convert(char, a.offence_date,22),4,2)+''/''+substring(convert(char, a.offence_date,22),1,2)+''/''+substring(convert(char, a.offence_date,22),7,len(a.offence_date)) as [trkh k''slhan],
		a.vehicle_type as [jenis kend],
		a.vehicle_no as [no kend],  
		a.road_tax_no as [no cukai jln],  
		c.description as [peruntukan undang2]  ,  
		b.[no] + '' '' + b.subsection_no  as [seksyen k''slhan], 
		a.offence_location_area as [kawasan],
		a.offence_location as [tpt/jln k''slhan]
			from
				dbo.offence_notice_maintenance a  
		inner join dbo.offence_section b   
		on a.offence_code = b.id and b.created_date = b.deleted_date
		inner join dbo.offence_act c  
		on c.code = b.offence_act_code  
		inner join dbo.officer_maintenance om  
		on om.id = a.officer_id  
		inner join dbo.officer_rank ur  
		on ur.code = om.[rank] ' + @wherequery +
				') as t1' + @fromto

	if @maxrows <> 0
		insert into @temptable exec sp_executesql @query,
				 		n'@startrowindex int, @maxrows int',
						@startrowindex = @firstrec,
						@maxrows = @lastrec
	else
		insert into @temptable exec sp_executesql @query
	
	select * from @temptable	
	
	set @query = 'select count(a.[notice_no]) as totalrecord from dbo.offence_notice_maintenance a  
		inner join dbo.offence_section b   
		on a.offence_code = b.id and b.created_date = b.deleted_date
		inner join dbo.offence_act c  
		on c.code = b.offence_act_code  
		inner join dbo.officer_maintenance om  
		on om.id = a.officer_id  
		inner join dbo.officer_rank ur  
		on ur.code = om.[rank] ' + @wherequery
	
	exec sp_executesql @query

    set nocount off;
end





create procedure [dbo].[getrep0003summary]
    @officerid varchar(100),
	@startdate datetime,
	@enddate datetime,
	@locationarea varchar(250),
	@locationdescription varchar(250)
as
begin

declare @query nvarchar(max)
set @query = 'select om.id, 
			convert ( varchar (8),ur.rank) + '' '' + convert ( varchar (8),om.rank_no ) + '' '' + om.name as [officerdetails],
			count(onm.notice_no) as [count],
			getdate() as [currentdate]
			from officer_maintenance om
			left join officer_rank ur
			on ur.code = om.[rank]
			left join offence_notice_maintenance onm
			on om.id = onm.officer_id
			where om.id = ''' + convert(varchar(20), @officerid) + '''' + 
			' and onm.offence_date >= ''' + convert(varchar(20), @startdate) + '''' + 
			' and onm.offence_date < ''' + convert(varchar(20), dateadd(day,1,@enddate)) + ''''

if (@locationarea <> '')
begin
	set @query = @query + 'and onm.offence_location in 
							(select [description]      
							from [dbo].[offence_location]
							where offence_location_area_code in (select code
							from [dbo].[offence_location_area]  
							where [description] = ''' + @locationarea + ''')'
	if (@locationdescription <> '')
	begin
		set @query = @query + 'and [description] =''' + @locationdescription + ''')'
	end
	else
	begin
		set @query = @query + ')'
	end

	set @query = @query + 'and onm.offence_location_area = ''' + @locationarea + ''''
end

set @query = @query + ' group by om.id, convert ( varchar (8),ur.rank) + '' '' + convert ( varchar (8),om.rank_no ) + '' '' + om.name'

--print @query

exec sp_executesql @query

end

set quoted_identifier on






-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getreportsforaccesslevel]
	-- add the parameters for the stored procedure here
	@accesslevel int
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select a.code,a.report_id,b.name,b.filename from dbo.report_access_maintenance a
	inner join dbo.report_maintenance b
	on a.report_id = b.code
	where a.access_code = @accesslevel and is_deleted =  0 order by b.[desc],
	cast( substring(b.code,8,5)as int)
	
end




-- =============================================  
-- author:  <author,,name>  
-- create date: <create date,,>  
-- description: <description,,>  
-- =============================================  
create procedure [dbo].[getsearchnoticepaging]  
 -- add the parameters for the stored procedure here  
 @startrowindex int,  
 @maxrows  int,  
 @wherequery  nvarchar(max),  
 @orderquery  nvarchar(max)  
as  
begin  
 -- set nocount on added to prevent extra result sets from  
 -- interfering with select statements.  
 set nocount on;

declare @firstrec int, @lastrec int
declare @fromto nvarchar(max)
	
if @maxrows <> 0
begin
	select @firstrec = ((@startrowindex - 1) * @maxrows) + 1
	select @lastrec = @startrowindex * @maxrows
		
	if (@startrowindex = 1)
	begin
		set @firstrec = 1
		set @lastrec = @maxrows
	end
		
	set @fromto = ' where rownumber between @startrowindex and @maxrows'
end
else
	set @fromto = ''
   
 declare @searchnoticetable table(no varchar(10), 
     compound_amount float,   
     offence_act_code int,  
     offence_section_code int,  
     offence_date datetime,
     vehicle_no varchar(20),  
     officer_code varchar(20),
     offence_location_area varchar(150),
     offence_location varchar(150))  
          
 declare @query nvarchar(max)  
 set @query = 'select   
     notice_no,
     compound_amount,
     offence_act_code,  
     offence_section_code,  
     offence_date,  
     vehicle_no,
     officer_id,
	 offence_location_area,
     offence_location 
    from (  
     select row_number() over (order by ' + @orderquery + ') as rownumber,  
      notice_no,
      offence_notice_maintenance.compound_amount,   
      offence_section.offence_act_code,  
      offence_section.code as offence_section_code,  
      offence_date,  
      vehicle_no,
      officer_id,
	  offence_notice_maintenance.offence_location_area,
      offence_location
     from  
      offence_notice_maintenance inner join offence_section on offence_notice_maintenance.offence_code = offence_section.id and offence_section.created_date = offence_section.deleted_date
	  ' + @wherequery +  
    ') as t1' + @fromto

   if @maxrows <> 0
		insert into @searchnoticetable exec sp_executesql @query,
				 		n'@startrowindex int, @maxrows int',
						@startrowindex = @firstrec,
						@maxrows = @lastrec
	else
	begin
		set @query = @query + ' order by ' + @orderquery

		insert into @searchnoticetable exec sp_executesql @query
	end

	print @query
   
 select * from @searchnoticetable   
   
 set @query = 'select count(notice_no) as totalrecord from dbo.offence_notice_maintenance inner join offence_section on offence_notice_maintenance.offence_code = offence_section.id and offence_section.created_date = offence_section.deleted_date ' + @wherequery  
   
 exec sp_executesql @query  
  
    set nocount off;  
end



-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getsetting]
	-- add the parameters for the stored procedure here
	
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select report_date from dbo.system_settings
end




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getuserdetails]
	-- add the parameters for the stored procedure here
as
begin
	select dbo.user_maintenance.code, dbo.user_maintenance.id, dbo.user_maintenance.name, 
	dbo.user_maintenance.access_level,
      (dbo.access_level.description),dbo.user_maintenance.last_login_date	
from dbo.user_maintenance
   inner join dbo.access_level on 
      dbo.user_maintenance.access_level = dbo.access_level.code
end















-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- exec getsearchresult 'null','null','1900-12-15 00:00:00.000','2008-12-15 00:00:00.000'
-- exec getsearchresult 'admin','','',''
-- exec getsearchresult '','su','10/1/2008 3:45:08 pm','10/30/2008 3:45:08 pm'
-- =============================================
create procedure [dbo].[getusersearchresult]
      -- add the parameters for the stored procedure here
      @id varchar(6),
      @name varchar(80),
      @startdate nvarchar(50),
      @enddate nvarchar(50)
as
begin
 declare @str1  varchar(500)

--declare @startdate datetime
--declare @enddate datetime

--set @startdate =convert (datetime , @strtdate, 101)
--print @startdate

--set @enddate =convert (datetime , @eddate, 101)
--print @enddate

if(@name = '')
set @str1 = ''
else
set @str1 ='a.name like''' + '%'+@name+'%'+''''
--print @str1

if(@id = '')
set @str1 = @str1+''

else if(@id != '' and @name = '')
 set @str1 = @str1 + ' a.id='''+@id+''''
else
set @str1 = @str1 + ' and  a.id='''+@id+''''

print @str1
if(@startdate = '' and @enddate = '')
set @str1 = @str1 + ''
else if(@startdate != '' and @enddate != '' and @id = '' and @name = '')
set @str1 = @str1 + 
--'a.created_date  between'''+@startdate+'''and'''+@enddate+''''
'a.created_date >='''+@startdate+''' and a.created_date<dateadd(day,1,'''+@enddate+''')'

else 
set @str1 = @str1 + 
--'and a.created_date  between'''+@startdate+'''and'''+@enddate+''''
'and a.created_date >='''+@startdate+''' and a.created_date<dateadd(day,1,'''+@enddate+''')'
print @str1
declare @str2 nvarchar(max)
--techm changed for date format on 17/12/09
set  @str2 = 'select a.code, a.id, a.name, 
      a.access_level,
      (b.description),
	  substring(convert(char,a.last_login_date,22),4,2)+''/''+substring(convert(char,a.last_login_date,22),1,2)+''/''+substring(convert(char,a.last_login_date,22),7,len(a.last_login_date)) as last_login_date
from dbo.user_maintenance as a
   inner join dbo.access_level as b on 
      a.access_level = b.code where  '+ @str1 + 'and a.deleted_date = a.created_date'

--insert into t1 values (@str1)
print @str2
exec (@str2)

end




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getusersearchresultpaging]
	-- add the parameters for the stored procedure here
	@startrowindex int,  
	@maxrows  int,  
	@wherequery  nvarchar(max)
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;
	declare @firstrec int, @lastrec int  
	select @firstrec = ((@startrowindex - 1) * @maxrows) + 1
	select @lastrec = @startrowindex * @maxrows

	if (@startrowindex = 1)
		begin
			set @firstrec = 1
			set @lastrec = @maxrows  
		end  
    -- insert statements for procedure here
	 declare @searchusertable table(code int,
	 name varchar(80),    
     id varchar(6),
     access_level smallint,  
     [description] varchar(32),
     [date] varchar(22))
     
     declare @query nvarchar(max)   
     set @query = 'select   
     code,     
     name,  
     id,  
     access_level,  
     [description],
     [date] from (  
     select row_number() over (order by a.code) as rownumber,  
      a.code, a.name,  a.id,
      a.access_level,
      (b.description),
	  substring(convert(char,a.last_login_date,22),4,2)+''/''+substring(convert(char,a.last_login_date,22),1,2)+''/''+substring(convert(char,a.last_login_date,22),7,len(a.last_login_date)) as [date]
from dbo.user_maintenance as a
   inner join dbo.access_level as b on 
      a.access_level = b.code '+ @wherequery + 'and a.deleted_date = a.created_date) as t1' + ' where rownumber between @startrowindex  and @maxrows'     

insert into @searchusertable exec sp_executesql @query,
				 	n'@startrowindex int,@maxrows int',
					@startrowindex = @firstrec,
					@maxrows = @lastrec

	select * from @searchusertable	
	
	set @query = 'select count(a.code) as totalrecord from dbo.user_maintenance as a
   inner join dbo.access_level as b on 
      a.access_level = b.code '+ @wherequery + 'and a.deleted_date = a.created_date'

	exec sp_executesql @query      
end







-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getusersforaccesslevel]
	-- add the parameters for the stored procedure here
	@accesslevel int
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select id, [name], access_level from dbo.user_maintenance
	where deleted_date is not null
	and access_level = @accesslevel
	
end











-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[getvehicletype]
	-- add the parameters for the stored procedure here
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	select rtrim(description) as [description], (code) as value 
	from vehicle_type
	where created_date = deleted_date
	order by [description]
end



create procedure [dbo].[insertnoticereporting]    
	@notices_issued  int,
    @notice_offence_date datetime,
    @description varchar(200),
    @offence_location_area varchar(200),
    @offence_location varchar(200),
    @offence_section_code tinyint,
    @offence_act_code tinyint
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	insert into [dbo].[notice_maintenance_reporting]
           ([notices_issued]
           ,[notice_offence_date]
           ,[description]
           ,[offence_location_area]
           ,[offence_location]
           ,[offence_section_code]
           ,[offence_act_code])
     values
           (@notices_issued,
			@notice_offence_date,
			@description,
			@offence_location_area,
			@offence_location,
			@offence_section_code,
			@offence_act_code)
end




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[updatelogintime] 
	@login varchar(20)
	as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;
	
   update dbo.user_maintenance 
	set last_login_date = getdate()
	where id = rtrim(ltrim(@login))
	


end









-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[updatenoticereporting]
	-- add the parameters for the stored procedure here
	@offencedate datetime
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	delete from dbo.notice_maintenance_reporting where notice_offence_date = @offencedate
end






-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[updatepassword] 
	@login varchar(20),
	@newpassword varchar(64)
	as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;
	
   update dbo.user_maintenance 
	set password = @newpassword,
	last_password_change_date = getdate()
	where id = rtrim(ltrim(@login))
	


end








-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[updatesentstatus]
	-- add the parameters for the stored procedure here
	@id uniqueidentifier
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	update offence_notice_maintenance
	set is_sent = 1, sent_date = getdate()
	where id = @id

end




-- =============================================
-- author:		<author,,name>
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[updatesetting]
	-- add the parameters for the stored procedure here
	@reportdate  date
as
begin
	-- set nocount on added to prevent extra result sets from
	-- interfering with select statements.
	set nocount on;

    -- insert statements for procedure here
	update dbo.system_settings set report_date = @reportdate
end




-- =============================================
-- author:		jesna
-- create date: <create date,,>
-- description:	<description,,>
-- =============================================
create procedure [dbo].[updateuserdetails]
	-- add the parameters for the stored procedure here
	@name varchar(80),
	@accesslevel int,
	@code int,
	@loggedinuser int
as
begin
	update user_maintenance set name = @name, access_level=@accesslevel,updated_by=@loggedinuser where (code = @code)
end
