<?php

// $theme = 'metronic-v52';
$theme = Config::get('site.theme');
View::addLocation(base_path() . '/resources/themes/' . $theme);
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('/welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/demo2', function() {
	return view('demo.demo2.index');
});
Route::get('/demo3', function() {
	return view('demo.demo3.index');
});
Route::get('/demo4', function() {
	return view('demo.demo4.index');
});
Route::get('/demo5', function() {
	return view('demo.demo5.index');
});
Route::get('/demo6', function() {
	return view('demo.demo6.index');
});
Route::get('/demo7', function() {
	return view('demo.demo7.index');
});
Route::get('/demo8', function() {
	return view('demo.demo8.index');
});
Route::get('/demo9', function() {
	return view('demo.demo9.index');
});
Route::get('/demo10', function() {
	return view('demo.demo10.index');
});
Route::get('/demo11', function() {
	return view('demo.demo11.index');
});
Route::get('/demo12', function() {
	return view('demo.demo12.index');
});
