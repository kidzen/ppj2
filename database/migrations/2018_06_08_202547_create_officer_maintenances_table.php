<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOfficerMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officer_maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code')->nullable(false)->unique();
            $table->string('report_id')->nullable(false);
            $table->integer('access_code')->nullable(false);

            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_by');
            $table->index('updated_by');
        });

        Schema::table('handheld_assignments', function (Blueprint $table) {
            $table->foreign('officer_code')->references('code')->on('officer_maintenances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officer_maintenances');
    }
}
