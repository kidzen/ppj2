<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHandheldMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handheld_maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code')->nullable(false)->unique();
            $table->string('serial_no')->nullable(false);
            $table->integer('status_code')->nullable(false);

            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_by');
            $table->index('updated_by');

            //Constraint
        });

        Schema::table('handheld_assignments', function (Blueprint $table) {
            $table->foreign('handheld_code')->references('code')->on('handheld_maintenances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('handheld_maintenances');
    }
}
