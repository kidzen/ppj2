<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenceRateMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offence_rate_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sno')->nullable(false)->unique();
            $table->integer('act_code')->nullable(false);
            $table->integer('section_code')->nullable(false);
            $table->integer('zone1');
            $table->decimal('amount1',20,2);
            $table->decimal('amount11',20,2);
            $table->string('amount_desc1');
            $table->integer('zone2');
            $table->decimal('amount2',20,2);
            $table->decimal('amount21',20,2);
            $table->string('amount_desc2');
            $table->integer('zone3');
            $table->decimal('amount3',20,2);
            $table->decimal('amount31',20,2);
            $table->string('amount_desc3');
            $table->integer('zone4');
            $table->decimal('amount4',20,2);
            $table->decimal('amount41',20,2);
            $table->string('amount_desc4');
            $table->decimal('max_compound_amount',20,2);

            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_by');
            $table->index('updated_by');
        });

        Schema::table('offence_rate_masters', function (Blueprint $table) {
            $table->foreign('act_code')->references('code')->on('offence_acts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offence_rate_masters');
    }
}
