<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenceNoticeMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offence_notice_maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->string('notice_no');
            $table->string('vehicle_no');
            $table->datetime('offence_date');
            $table->string('officer_id');
            $table->integer('handheld_code');
            $table->string('vehicle_type');
            $table->string('vehicle_color');
            $table->string('vehicle_make_model');
            $table->string('road_tax_no');
            $table->string('offence_code');
            $table->string('offence_location_area');
            $table->string('offence_location');
            $table->string('offence_location_details');
            $table->float('compound_amount');
            $table->date('compound_expiry_date');
            $table->string('witness');
            $table->string('image_name1');
            $table->string('image_name2');
            $table->string('image_name3');
            $table->string('offence_notice_status');
            $table->integer('is_sent');
            $table->date('sent_date');

            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_by');
            $table->index('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offence_notice_maintenances');
    }
}
