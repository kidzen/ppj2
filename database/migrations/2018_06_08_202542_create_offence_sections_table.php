<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffenceSectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offence_sections', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offence_act_code')->nullable(false);
            $table->integer('code')->nullable(false);
            $table->string('no')->nullable(false);
            $table->string('subsection_no');
            $table->string('description')->nullable(false);

            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_by');
            $table->index('updated_by');
        });

        // Schema::table('offence_rate_masters', function (Blueprint $table) {
        //     $table->foreign(['act_code','section_code'])->references(['offence_act_code','code'])->on('offence_sections');
        // });
        Schema::table('offence_sections', function (Blueprint $table) {
            $table->foreign('offence_act_code')->references('code')->on('offence_acts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offence_sections');
    }
}
