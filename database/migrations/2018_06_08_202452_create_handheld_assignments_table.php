<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHandheldAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handheld_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code')->nullable(false)->unique();
            $table->integer('handheld_code')->nullable(false);
            $table->integer('officer_code');
            $table->datetime('start_assignment_date')->nullable(false);
            $table->datetime('end_assignment_date')->nullable(false);
            $table->integer('handheld_status_code');

            // default column
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();

            // contstraint

            // index
            // $table->index('code');
            // $table->index('handheld_code');
            $table->index('handheld_status_code');
            $table->index('created_by');
            $table->index('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('handheld_assignments');
    }
}

