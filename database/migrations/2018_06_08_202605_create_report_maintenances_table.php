<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_maintenances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code')->nullable(false)->unique();
            $table->string('name');
            $table->string('filename');

            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_by');
            $table->index('updated_by');
        });

        Schema::table('report_access_maintenances', function (Blueprint $table) {
            $table->foreign('report_id')->references('code')->on('report_maintenances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_maintenances');
    }
}
