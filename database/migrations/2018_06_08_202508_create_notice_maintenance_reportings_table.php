<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticeMaintenanceReportingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notice_maintenance_reportings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notices_issued')->nullable(false);
            $table->datetime('notice_offence_date')->nullable(false);
            $table->string('description');
            $table->string('offence_location_area');
            $table->string('offence_location');
            $table->integer('offence_act_code');
            $table->integer('offence_section_code');

            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_by');
            $table->index('updated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notice_maintenance_reportings');
    }
}
