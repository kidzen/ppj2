create table [dbo].[access_level](
	[code] [smallint] identity(1,1) not null,
	[description] [varchar](32) not null,
	[created_by] [varchar](32) not null constraint [df__access_le__creat__4589517f]  default (user_name()),
	[created_date] [datetime] not null constraint [df__access_le__creat__467d75b8]  default (getdate()),
 constraint [pk_access_level] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[handheld_assignment](
	[code] [int] identity(1,1) not null,
	[handheld_code] [int] null,
	[officer_code] [int] null,
	[start_assignment_date] [datetime] not null,
	[end_assignment_date] [datetime] not null,
	[handheld_status_code] [smallint] null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
 constraint [pk_handheld_assignment] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary],
 constraint [un_handheld_code_assignment] unique nonclustered 
(
	[handheld_code] asc,
	[start_assignment_date] asc,
	[end_assignment_date] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary],
 constraint [un_officer_code_assignment] unique nonclustered 
(
	[officer_code] asc,
	[start_assignment_date] asc,
	[end_assignment_date] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[handheld_maintenance](
	[code] [int] identity(1,1) not null,
	[id] [varchar](3) not null,
	[serial_no] [varchar](19) not null,
	[status_code] [smallint] not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_handheld_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[handheld_status](
	[code] [smallint] identity(1,1) not null,
	[description] [varchar](32) not null,
	[created_by] [varchar](32) not null constraint [df__handheld___creat__4e1e9780]  default (user_name()),
	[created_date] [datetime] not null constraint [df__handheld___creat__4f12bbb9]  default (getdate()),
 constraint [pk_handheld_status] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[notice_maintenance_reporting](
	[id] [int] identity(1,1) not null,
	[notices_issued] [int] not null,
	[notice_offence_date] [datetime] not null,
	[description] [varchar](200) null,
	[offence_location_area] [varchar](200) null,
	[offence_location] [varchar](200) null,
	[offence_act_code] [tinyint] null,
	[offence_section_code] [tinyint] null,
 constraint [pk_notice_maintenance_reporting] primary key clustered 
(
	[id] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_act](
	[code] [int] identity(1,1) not null,
	[description] [varchar](512) not null,
	[short_description] [varchar](128) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_offence_act] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_location](
	[offence_location_area_code] [int] not null,
	[code] [int] not null,
	[description] [varchar](100) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_offence_location] primary key clustered 
(
	[offence_location_area_code] asc,
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_location_area](
	[code] [int] identity(1,1) not null,
	[description] [varchar](50) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_offence_location_area] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_notice_maintenance](
	[id] [uniqueidentifier] not null,
	[notice_no] [varchar](20) null,
	[vehicle_no] [varchar](20) null,
	[offence_date] [datetime] null,
	[officer_id] [varchar](10) null,
	[handheld_code] [int] null,
	[vehicle_type] [varchar](20) null,
	[vehicle_color] [varchar](20) null,
	[vehicle_make_model] [varchar](100) null,
	[road_tax_no] [varchar](20) null,
	[offence_code] [varchar](20) null,
	[offence_location_area] [varchar](50) null,
	[offence_location] [varchar](100) null,
	[offence_location_details] [varchar](100) null,
	[compound_amount] [float] null,
	[compound_expiry_date] [date] null,
	[witness] [varchar](100) null,
	[image_name1] [varchar](30) null,
	[image_name2] [varchar](30) null,
	[image_name3] [varchar](30) null,
	[created_date] [date] null,
	[offence_notice_status] [char](1) null,
	[is_sent] [bit] null,
	[sent_date] [datetime] null,
 constraint [pk_offence_notice_maintenance] primary key clustered 
(
	[id] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_rate_master](
	[sno] [int] identity(1,1) not null,
	[act_code] [int] not null,
	[section_code] [int] not null,
	[zone1] [int] null,
	[amount1] [decimal](18, 2) null,
	[amount11] [decimal](18, 2) null,
	[amount_desc1] [varchar](100) null,
	[zone2] [int] null,
	[amount2] [decimal](18, 2) null,
	[amount21] [decimal](18, 2) null,
	[amount_desc2] [varchar](100) null,
	[zone3] [int] null,
	[amount3] [decimal](18, 2) null,
	[amount31] [decimal](18, 2) null,
	[amount_desc3] [varchar](100) null,
	[zone4] [int] null,
	[amount4] [decimal](18, 2) null,
	[amount41] [decimal](18, 2) null,
	[amount_desc4] [varchar](100) null,
	[max_compound_amount] [decimal](18, 2) null,
 constraint [pk_offence_rate_master] primary key clustered 
(
	[sno] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[offence_section](
	[offence_act_code] [int] not null,
	[code] [int] not null,
	[id] [varchar](20) not null,
	[no] [varchar](24) not null,
	[subsection_no] [varchar](24) null,
	[description] [varchar](1024) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_offence_section] primary key clustered 
(
	[offence_act_code] asc,
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[officer_maintenance](
	[code] [int] identity(1,1) not null,
	[id] [varchar](10) not null,
	[rank] [smallint] not null,
	[rank_no] [varchar](6) null,
	[name] [varchar](80) not null,
	[password] [varchar](64) not null,
	[password_changed_by] [int] not null,
	[last_password_change_date] [datetime] not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_officer_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[officer_rank](
	[code] [smallint] identity(1,1) not null,
	[rank] [varchar](20) not null,
	[has_rank_no] [bit] not null,
	[created_by] [varchar](32) not null constraint [df__officer_r__creat__3429bb53]  default (user_name()),
	[created_date] [datetime] not null constraint [df__officer_r__creat__351ddf8c]  default (getdate()),
 constraint [pk_officer_rank] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[report_access_maintenance](
	[code] [int] identity(1,1) not null,
	[report_id] [varchar](12) not null,
	[access_code] [smallint] not null,
	[created_by] [int] null,
	[created_date] [datetime] not null,
 constraint [pk_report_access_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[report_maintenance](
	[code] [varchar](12) not null,
	[name] [varchar](80) null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[is_deleted] [bit] null constraint [df_report_maintenance_is_deleted]  default ((0)),
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[filename] [varchar](80) null,
	[desc] [int] null,
 constraint [pk_report_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[system_settings](
	[report_date] [date] null
) on [primary]


create table [dbo].[user_maintenance](
	[code] [int] identity(1,1) not null,
	[id] [varchar](6) not null,
	[name] [varchar](80) not null,
	[password] [varchar](64) not null,
	[access_level] [smallint] not null,
	[last_login_date] [datetime] not null,
	[last_password_change_date] [datetime] not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_user_maintenance] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[vehicle_make](
	[code] [int] identity(1,1) not null,
	[description] [varchar](128) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_vehicle_make] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[vehicle_model](
	[vehicle_make_code] [int] not null,
	[code] [int] not null,
	[description] [varchar](64) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_vehicle_model] primary key clustered 
(
	[vehicle_make_code] asc,
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

create table [dbo].[vehicle_type](
	[code] [int] identity(1,1) not null,
	[description] [varchar](64) not null,
	[created_by] [int] not null,
	[created_date] [datetime] not null,
	[updated_by] [int] not null,
	[updated_date] [datetime] not null,
	[deleted_by] [int] not null,
	[deleted_date] [datetime] not null,
 constraint [pk_vehicle_type] primary key clustered 
(
	[code] asc
)with (pad_index = off, statistics_norecompute = off, ignore_dup_key = off, allow_row_locks = on, allow_page_locks = on) on [primary]
) on [primary]

-- alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___creat__7073af84] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___handh__22401542] foreign key([handheld_code])
references [dbo].[handheld_maintenance] ([code])


alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___handh__725bf7f6] foreign key([handheld_status_code])
references [dbo].[handheld_status] ([code])


alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___offic__73501c2f] foreign key([officer_code])
references [dbo].[officer_maintenance] ([code])


-- alter table [dbo].[handheld_assignment]  with check add  constraint [fk__handheld___updat__74444068] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[handheld_maintenance]  with check add  constraint [fk__handheld___creat__1b9317b3] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[handheld_maintenance]  with check add  constraint [fk__handheld___delet__1d7b6025] foreign key([deleted_by])
-- references [dbo].[user_maintenance] ([code])


alter table [dbo].[handheld_maintenance]  with check add  constraint [fk__handheld___statu__1a9ef37a] foreign key([status_code])
references [dbo].[handheld_status] ([code])


-- alter table [dbo].[handheld_maintenance]  with check add  constraint [fk__handheld___updat__1c873bec] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[offence_act]  with check add  constraint [fk__offence_a__creat__7dcdaaa2] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[offence_act]  with check add  constraint [fk__offence_a__delet__7ec1cedb] foreign key([deleted_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[offence_act]  with check add  constraint [fk__offence_a__updat__7fb5f314] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[offence_location]  with check add  constraint [fk__offence_l__creat__056ecc6a] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[offence_location]  with check add  constraint [fk__offence_l__delet__0662f0a3] foreign key([deleted_by])
-- references [dbo].[user_maintenance] ([code])


alter table [dbo].[offence_location]  with check add  constraint [fk__offence_l__offen__075714dc] foreign key([offence_location_area_code])
references [dbo].[offence_location_area] ([code])


-- alter table [dbo].[offence_location]  with check add  constraint [fk__offence_l__updat__084b3915] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[offence_location_area]  with check add  constraint [fk__offence_l__creat__093f5d4e] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[offence_location_area]  with check add  constraint [fk__offence_l__delet__0a338187] foreign key([deleted_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[offence_location_area]  with check add  constraint [fk__offence_l__updat__0b27a5c0] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])


alter table [dbo].[offence_rate_master]  with check add  constraint [fk_offence_rate_master_act_code] foreign key([act_code])
references [dbo].[offence_act] ([code])


alter table [dbo].[offence_rate_master]  with check add  constraint [fk_offence_rate_master_offence_section] foreign key([act_code], [section_code])
references [dbo].[offence_section] ([offence_act_code], [code])


-- alter table [dbo].[offence_section]  with check add  constraint [fk__offence_s__creat__178d7ca5] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[offence_section]  with check add  constraint [fk__offence_s__delet__1881a0de] foreign key([deleted_by])
-- references [dbo].[user_maintenance] ([code])


alter table [dbo].[offence_section]  with check add  constraint [fk__offence_s__offen__1975c517] foreign key([offence_act_code])
references [dbo].[offence_act] ([code])


-- alter table [dbo].[offence_section]  with check add  constraint [fk__offence_s__updat__1a69e950] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])


alter table [dbo].[report_access_maintenance]  with check add  constraint [fk_ram_access_code__al_code] foreign key([access_code])
references [dbo].[access_level] ([code])


-- alter table [dbo].[report_access_maintenance]  with check add  constraint [fk_ram_created_by__um_code] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


alter table [dbo].[report_access_maintenance]  with check add  constraint [fk_ram_report_id__rm_code] foreign key([report_id])
references [dbo].[report_maintenance] ([code])


-- alter table [dbo].[report_maintenance]  with check add  constraint [fk_rm_created_by__um_code] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[report_maintenance]  with check add  constraint [fk_rm_updated_by__um_code] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])


alter table [dbo].[user_maintenance]  with check add  constraint [fk__user_main__acces__23f3538a] foreign key([access_level])
references [dbo].[access_level] ([code])


-- alter table [dbo].[vehicle_make]  with check add  constraint [fk__vehicle_m__creat__24e777c3] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[vehicle_make]  with check add  constraint [fk__vehicle_m__delet__25db9bfc] foreign key([deleted_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[vehicle_make]  with check add  constraint [fk__vehicle_m__updat__26cfc035] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[vehicle_model]  with check add  constraint [fk__vehicle_m__creat__27c3e46e] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[vehicle_model]  with check add  constraint [fk__vehicle_m__delet__28b808a7] foreign key([deleted_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[vehicle_model]  with check add  constraint [fk__vehicle_m__updat__29ac2ce0] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])


alter table [dbo].[vehicle_model]  with check add  constraint [fk__vehicle_m__vehic__2aa05119] foreign key([vehicle_make_code])
references [dbo].[vehicle_make] ([code])


-- alter table [dbo].[vehicle_type]  with check add  constraint [fk__vehicle_t__creat__2b947552] foreign key([created_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[vehicle_type]  with check add  constraint [fk__vehicle_t__delet__2c88998b] foreign key([deleted_by])
-- references [dbo].[user_maintenance] ([code])


-- alter table [dbo].[vehicle_type]  with check add  constraint [fk__vehicle_t__updat__2d7cbdc4] foreign key([updated_by])
-- references [dbo].[user_maintenance] ([code])



